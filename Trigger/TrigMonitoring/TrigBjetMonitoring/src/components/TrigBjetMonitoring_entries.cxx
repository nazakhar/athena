/* 
Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration 
*/
#include "TrigBjetMonitoring/HLTBjetMonTool.h"
#include "../TrigBjetMonitorAlgorithm.h"

DECLARE_COMPONENT( HLTBjetMonTool )
DECLARE_COMPONENT( TrigBjetMonitorAlgorithm )

