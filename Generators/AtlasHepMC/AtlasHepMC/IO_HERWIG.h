/* Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
   Author: Andrii Verbytskyi andrii.verbytskyi@mpp.mpg.de
*/
#ifndef ATLASHEPMC_IOHERWIG_H
#define ATLASHEPMC_IOHERWIG_H
#include "HepMC/IO_HERWIG.h"
#endif
